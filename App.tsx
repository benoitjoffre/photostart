import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack'
import {Onboarding, Welcome} from './src/Authentication'
import { LoadAssets } from './src/components';

const fonts = {
  "Roboto-Regular": require('./assets/fonts/Roboto-Regular.ttf'),
  "Roboto-Bold": require('./assets/fonts/Roboto-Bold.ttf'),
  "Roboto-Medium": require('./assets/fonts/Roboto-Medium.ttf'),
  "Roboto-Light": require('./assets/fonts/Roboto-Light.ttf')
}

const AuthenticationStack = createStackNavigator()
const AuthenticationNavigator = () => (
  <AuthenticationStack.Navigator headerMode="none">
    <AuthenticationStack.Screen name="OnBoarding" component={Onboarding} />
    <AuthenticationStack.Screen name="Welcome" component={Welcome} />
  </AuthenticationStack.Navigator>
  );
  console.disableYellowBox = true;
const App = () => (
  <LoadAssets {...{ fonts }}>
    <AuthenticationNavigator />
  </LoadAssets>
)
  
export default App;