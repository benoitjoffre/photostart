import React from 'react'
import {View, Text, StyleSheet, Dimensions} from 'react-native'
import Button from '../../components/Button'
const { width } = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        alignItems: "center",
        padding: 44,
        width,
    },
    subtitle: {
        fontFamily: "Roboto-Bold",
        fontSize: 24,
        lineHeight: 30,
        marginBottom: 12,
        color: "#0C0D34"
    },
    description: {
        fontFamily: "Roboto-Regular",
        fontSize: 16,
        lineHeight: 24,
        color: "#0C0D34",
        textAlign: "center",
        marginBottom: 40,
    }
})
interface SubslideProps {
    subtitle: string;
    description: string;
    last?: boolean;
    onPress: () => void;
}
const Subslide = ({subtitle, description, last, onPress}: SubslideProps) => {
return (
    <View style={styles.container}>
        <Text style={styles.subtitle}>{subtitle}</Text>
        <Text style={styles.description}>{description}</Text>
        <Button 
        label={last ? "Lets get started": "Next"} 
        variant={last ? "primary": "default"}
        {...{onPress}}
        />
    </View>
)
}

export default Subslide;