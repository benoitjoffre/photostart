import React, {useRef} from 'react'
import {View, StyleSheet, Dimensions} from 'react-native'
import { interpolateColor, useScrollHandler } from 'react-native-redash'
import Animated, {multiply, divide} from 'react-native-reanimated'
import Subslide from './Subslide'
import Slide from './Slide'
import Dot from '../../components/Dot'
const BORDER_RADIUS = 75;
const { width } = Dimensions.get("window")
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white"
    },
    slider: {
        flex: 1.1,
        borderBottomRightRadius: BORDER_RADIUS,
    },
    footer: {
        flex:1
    },
    footerContent: {
        flex: 1, 
        flexDirection: "row",
        backgroundColor: "white", 
        borderTopLeftRadius: BORDER_RADIUS
    },
    pagination: {
        ...StyleSheet.absoluteFillObject, 
        height: BORDER_RADIUS,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    }
})
const slides = [
    {
        title: "Relaxed", 
        subtitle: "Subtitle 1", 
        description: "Ac ne quis a nobis hoc ita dici forte miretur, quod alia quaedam in hoc facultas sit ingeni, neque haec", 
        color: "#BFEAF5"
    },
    {
        title: "Playful", 
        subtitle: "Subtitle 2", 
        description: "dicendi ratio aut disciplina, ne nos quidem huic uni studio penitus umquam dediti fuimus.", 
        color: "#BEECC4"
    },
    {
        title: "Excentric", 
        subtitle: "Subtitle 3", 
        description: "Etenim omnes artes, quae ad humanitatem pertinent, habent quoddam commune vinculum, et quasi cognatione quadam inter se continentur.", 
        color: "#FFE4D9"
    },
    {
        title: "Funky", 
        subtitle: "Subtitle 4", 
        description: "Accenderat super his incitatum propositum ad nocendum aliqua mulier vilis, quae ad palatium ut poposcerat",  
        color: "#FFDDDD"
    },
]
const Onboarding = () => {
    
    const scroll = useRef<Animated.ScrollView>(null);
    const {scrollHandler, x} = useScrollHandler()

    const backgroundColor  = interpolateColor(x,{
        inputRange: slides.map((_,i) => i * width),
        outputRange: slides.map( slide => slide.color)
    })
    return (
        <View style={styles.container}>
            <Animated.View style={[styles.slider, { backgroundColor }]}>
                <Animated.ScrollView 
                ref={scroll}
                horizontal 
                snapToInterval={width} 
                decelerationRate="fast" 
                showsHorizontalScrollIndicator={false} 
                bounces={false}
                {...scrollHandler}
                >
                    {slides.map(({title}, index) => (
                        <Slide key={index} right={!!(index % 2)} {...{title}} />
                    ))}
                    
                </Animated.ScrollView>
            </Animated.View>
            <View style={styles.footer}>
                <Animated.View style={{...StyleSheet.absoluteFillObject, backgroundColor}} />
                <Animated.View style={styles.footerContent}>
                    <View style={styles.pagination}>
                        {slides.map((_, index) => (
                            <Dot key={index} currentIndex={divide(x, width)}{...{index}} />
                        ))}
                    </View>
                    <Animated.View style={{
                        flex: 1, 
                        flexDirection: "row",
                        width: width * slides.length,
                        transform: [{translateX: multiply(x, -1)}]
                    }}>

                    {slides.map(({ subtitle, description }, index) => (
                        <Subslide 
                        key={index} 
                        onPress={() => {if(scroll.current) {scroll.current.getNode().scrollTo({x: width * (index + 1), animated: true})}}}
                        last={index === slides.length - 1} 
                        {...{subtitle, description }} />
                        ))}
                        </Animated.View>
                </Animated.View>
            </View>
        </View>
    )
}

export default Onboarding;